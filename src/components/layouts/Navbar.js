import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'; 

import './../../style.css'



const Navbar = () => {
	return (
	
			<nav className="navbar navbar-dark navbar-expand-sm">
				<Link to="/" className="navbar-brand" id="navtitle"> AdviceShares.com </Link> 
                <button 
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNav">
                    <span class="navbar-toggler-icon"></span> 
                </button> 
                <div className="collapse navbar-collapse" id="navbarNav"> 
                    <ul className="navbar-nav ml-auto"> 

                    { localStorage.getItem('isLogged') && localStorage.getItem('role') == "admin" ? 
                         <Fragment>
                         <li className="nav-item">
                           <Link to="/admin-panel" className="nav-link">Admin Panel</Link>
                         </li> 

                         <li className="nav-item">
							<Link to="/appointment" className="nav-link">Appointment</Link>
						</li>
                          <li className="nav-item">
                          <Link to="/transaction" className="nav-link">Transaction</Link>
                      </li> 

                         <li className="nav-item">
                        <Link className="nav-link" onClick={()=>{
                        localStorage.clear()
                        window.location.href = "/"
                        }}>LogOut</Link>
                        </li>

                         </Fragment>
                        :
                        <Fragment> 
                        { localStorage.getItem('isLogged') && localStorage.getItem('role') == "user" ?
                        <Fragment> 
                            <li className="nav-item"> 
                         <Link to="/home" className="nav-link">Welcome {localStorage.getItem('user')}  </Link> 
                     </li> 

                     <li className="nav-item"> 
                            <Link to="/Catalog" className="nav-link">Catalog</Link> 
                        </li> 

                        <li className="nav-item">
							<Link to="/appointment" className="nav-link">Appointment</Link>
						</li>
                          <li className="nav-item">
                          <Link to="/transaction" className="nav-link">Transaction</Link>
                      </li> 

                        <li className="nav-item">
                        <Link className="nav-link" onClick={()=>{
                        localStorage.clear()
                        window.location.href = "/"
                        }}>LogOut</Link>
                        </li>
                        </Fragment>
                        :
                        <Fragment>
                        <li className="nav-item"> 
                            <Link to="/" className="nav-link">Home</Link> 
                        </li> 

                        <li className="nav-item"> 
                            <Link to="/login" className="nav-link">Login</Link> 
                        </li>

                        <li className="nav-item"> 
                            <Link to="/register" className="nav-link">Register</Link> 
                        </li>
                        </Fragment>
                        }
                     
                        
                       
                      </Fragment>
                        }

                      
                        
                        
                        
                        
                    

                    </ul>
                </div>
			</nav>
	
	);
}

export default Navbar;