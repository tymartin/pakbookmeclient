import React, { Fragment } from 'react'; 


const Home = ({advisers}) => {
    return (
        <Fragment>
                 <div className="container">
                    <div class="jumbotron jumbotron-fluid"id="cjumbo">
                        <div class="container">
                            <h5 class="display-4 text-center py-8">Connect with Us!</h5> 
                         </div>
                    </div> 

                    <div className="row">
              
              {
              advisers.map( adviser => {
                  return ( 
                       
                              <div className="col-12 col-md-3 mb-4">
                              <div className="card" id="cartcard">
                              <img src={"https://bookmaster-server.herokuapp.com" +  adviser.image} height="98"/>
                                      <div className="card-header" id="cartcardhead"><strong><center>{adviser.name}</center></strong>
                                      </div>
                                      <div className="card-body">
                                          <p className="card-item">Counseling Fee: <strong>{adviser.price} </strong>  </p>
                                          <p className="card-item mb-0">Counseling Type:</p>
                                          <p className="card-item" id="carddesc"><strong>{adviser.description}</strong></p>
                                          <p className="card-item mb-0">Schedule:</p>
                                          <p className="card-item">{adviser.schedule  }</p>
                                          
                                      </div>
                                  </div> 
                               </div>         
                          )
                      })
                  }    
          
      
          
      </div>
  </div>
  </Fragment>
                  
    )
}

export default Home; 