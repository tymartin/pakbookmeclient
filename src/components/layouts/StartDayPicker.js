import React, { useState } from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

const StartDayPicker = ({selectedDays, setSelectedDays}) => {

   

	const handleDayClick= (day, { selected, disabled }) => {

		if (disabled) { return }

		let updatedDays = [...selectedDays]

		if (selected) {

			const selectedIndex = selectedDays.findIndex( selectedDay => {
				DateUtils.isSameDay(selectedDay, day)
			})

			updatedDays.splice(selectedIndex,1)

		} else {
			updatedDays = [...updatedDays, day]
		}

		setSelectedDays(updatedDays);
	}

	return(
		<React.Fragment>
			<DayPicker 
                onDayClick={handleDayClick}
				selectedDays={selectedDays}
				disabledDays={
					[
						{daysOfWeek : [0,6]},
						new Date(2020,1,12),
						{before : new Date}
					]
				}
			/>
		</React.Fragment>
	)
}

export default StartDayPicker