import React, { Fragment } from 'react'; 
import counseling from './../counseling.jpg'; 
import { Link } from 'react-router-dom';
import StartDayPicker from './layouts/StartDayPicker'; 

const Catalog = ({advisers, handleAddToCart, selectedDays, setSelectedDays}) => {
    return (
        <Fragment>
        <div className="container">
             <div class="jumbotron jumbotron-fluid"id="cjumbo">
                        <div class="container">
                            <h5 class="display-4 text-center py-8  ">Our Advisers</h5> 
                         </div>
                    </div> 
                <div className="container">
                <div className="card col-md-5 mb-2 bg-secondary" >
                <h3 className="card header" id="calendarbod"><center>Select Appointment Date </center> </h3> 
                <StartDayPicker
                selectedDays={selectedDays}
                setSelectedDays={setSelectedDays}
				/> 
                </div> 
                </div> 
            <div className="row">
              
                    {
					advisers.map( adviser => {
						return ( 
                             
                                    <div className="col-12 col-md-3 mb-4">
                                    <div className="card" id="cartcard">
                                    <img src={"https://bookmaster-server.herokuapp.com" +  adviser.image} height="98"/>
                                            <div className="card-header" id="cartcardhead"><strong><center>{adviser.name}</center></strong>
                                            </div>
                                            <div className="card-body">
                                                <p className="card-item">Counseling Fee: <strong>{adviser.price} </strong>  </p>
                                                <p className="card-item mb-0">Counseling Type:</p>
                                                <p className="card-item" id="carddesc"><strong>{adviser.description}</strong></p>
                                                <p className="card-item mb-0">Schedule:</p>
                                                <p className="card-item">{adviser.schedule  }</p>
                                                <button onClick={()=>{handleAddToCart(adviser)}} className="btn" id="addproductbtn" type="submit"><center>Book A Session</center></button>
                                            </div>
                                        </div> 
                                     </div>         
                                )
                            })
                        }    
                
            
                
            </div>
        </div>
		</Fragment>
                    
             

				
        
    )
}

export default Catalog; 