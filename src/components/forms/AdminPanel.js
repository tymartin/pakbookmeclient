import React from "react"
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"


import AddProductForm from './AddProductForm'; 
import EditProductForm from './EditProductForm';
import DeleteProductForm from './DeleteProductForm';  
import AddCategoryForm from './AddCategoryForm';
import EditCategoryForm from './EditCategoryForm'; 
import DeleteCategoryForm from './DeleteCategoryForm'; 

const AdminPanel = ({categories, handleChangeCategoriesStatus, categoriesStatus, advisers, handleChangeAdvisersStatus, advisersStatus}) => {

	return (

		<div className="container">
			<div className="row">
				<div className=" col-md-4 pt-3 mt-3">       	
         	        <AddProductForm categories={categories}/>   	
         	 	</div>
         	 	<div className="col-md-4 pt-3 mt-3"> 
					 <EditProductForm 
					 categories={categories}
					 advisers={advisers}
					 handleChangeAdvisersStatus={handleChangeAdvisersStatus}
					 advisersStatus={advisersStatus}
	
					 /> 

                    <DeleteProductForm advisers={advisers}/> 
         	 	</div> 

                  <div className=" col-md-4 pt-3 mt-3">       	
         	        <AddCategoryForm/>  

                    <EditCategoryForm
					categories={categories}
					handleChangeCategoriesStatus={handleChangeCategoriesStatus}
					categoriesStatus={categoriesStatus}
					/>   	

                    <DeleteCategoryForm 
					categories={categories}
					/>   	 	
         	 	</div>
         	 	
         	</div> 
        </div> 
		)
}


export default AdminPanel; 
