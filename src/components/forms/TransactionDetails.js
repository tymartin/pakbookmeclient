import React, {useState} from 'react'; 



const TransactionDetails = ({transaction, advisers}) => {

const[status, setStatus] = useState({
	status : transaction.status
});

const handleChangeSelectStatus = e => {
	setStatus({status : e.target.value})
}

const handleEditStatus = (transactionId) => {
	fetch('https://bookmaster-server.herokuapp.com/transactions/'+transactionId,{
	method : "PUT",
	headers : {
	"Content-Type" : "application/json",
	"Authorization" : localStorage.getItem('token')
},
body : JSON.stringify(status)
	}).then(data => {
	data.json()
	}).then(result => {
	console.log(result)
	})
	}


 return (	
 						<React.Fragment> 
 						<p className="mb-0"> Payment Mode : <strong>{transaction.paymentMode}</strong></p> 
 						<p className="mb-0"> Client Name :<strong>{transaction.userId}</strong></p>


						<p className="mb-0"> Status :<strong>{transaction.status}</strong></p>
						  	<select onChange={handleChangeSelectStatus} name="status" className="form-control w-25 mt-2 mb-2">
						  		<option value="Pending"> Pending</option> 
						  		<option value="Canceled">Canceled </option> 
						  		<option value="Done">Done</option> 
						  	</select>  
						  	<button onClick={()=>{handleEditStatus(transaction._id)}} className="btn btn-warning mb-2">Update Status</button>
					
 						<table class="table table-striped">

						  <thead>
						    <tr>
						      <th scope="col">Item Name</th>
						      <th scope="col">Price</th>
						      <th scope="col">Subtotal</th>
						    </tr>
						  </thead>
						  <tbody>
						  	
								{
									transaction.advisers.map(adviser=> {
										return(
											<tr>
									      		<td>{adviser.name}</td>
									      		<td>{adviser.price}</td>
									      		<td>{adviser.subtotal}</td>
							   		 		</tr>
							   		 	)
									})
								}
						  		
					
						  </tbody>
						  <tfoot> 
						  	<td className="text-right"> Total: </td> 
						  	<td><strong> {transaction.total}</strong> </td> 
						  	<td></td>
						  </tfoot> 
						</table>
						</React.Fragment> 
 )

}

export default TransactionDetails;
