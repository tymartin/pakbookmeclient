import React, {useState}  from "react"


const AddCategoryForm = () => { 

	const [formData, setFormData] = useState("")

	const handleCategoryNameChange = (e) => {  
		setFormData(e.target.value)
	}

	const handleAddCategory = (e) => {
		e.preventDefault()

		let url = "https://bookmaster-server.herokuapp.com/categories/";
		let data = {name: formData}
		let token =localStorage.getItem('token'); 
		fetch(url, {
			method :"POST",
			body: JSON.stringify(data), 
			headers: {
				"Content-Type" : "application/json", 
				"Authorization" : token
			}
		})
		.then( data=>data.json())
		.then( category=>console.log(category))
	}


	return(

		<div class="accordion pr-3" id="accordionDelProd">
			  <div class="card" id="addproduct">
			  <div class="card-header" id="headingOne">
				 <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOneAddCat" aria-expanded="true" aria-controls="collapseOne"><center>Add New Career	</center>	</h5> 
			   </div> 

			   <div id="collapseOneAddCat" class="collapse-show" aria-labelledby="headingOne" data-parent="#collapseOneAddCat">
				<div class="card-body text-light"> 	   
			<form action=""  onSubmit={handleAddCategory}> 
				<div className="form-group text-light"> 

					<input
						type="text"
						className="form-control mb-1"
						id="name"
						name="name"
						autoComplete="off"
						placeholder="Enter New Career"
						onChange={handleCategoryNameChange}

						/>
						</div> 
							
			
						<button className="btn mb-4 float-right" id="addproductbtn"> Add Career</button>

			</form> 
			</div> 
			</div> 
			</div> 
		</div>
	 )
}


export default AddCategoryForm; 
