import React, {useState} from "react"


const EditCategoryForm = ({categories, handleChangeCategoriesStatus, categoriesStatus}) => {

	const [selectedCategory, setSelectedCategory] = useState({
		name: null, 
		id: null, 
	
	})
	
		const handleChangeSelected = (e) => {
			let categorySelected = categories.find(category => { 
			if(category._id == e.target.value) {
				return category.name
				}
			})
			setSelectedCategory({
				id: e.target.value,
				name: categorySelected.name
			})
		}
	
		const handleChangeName = e => {
			setSelectedCategory ({
				...selectedCategory,
				[e.target.name] : e.target.value
			})
		}
	
		const handleEditCategory = e => {
			e.preventDefault(); 
	
			fetch("https://bookmaster-server.herokuapp.com/categories/" + selectedCategory.id, {
				method: "PUT",
				body : JSON.stringify({ name:selectedCategory.name}), 
				headers: {
					"content-Type": "application/json",
					"Authorization" : localStorage.getItem('token')
				}
			})
			.then(data => {
				return data.json()})
			.then(result => 
				handleChangeCategoriesStatus({
					lastUpdated : selectedCategory.id,
					status: null, 
					isLoading: true
				})
			)
		}
	

	return(

		<div class="accordion pr-3 mt-2" id="accordionEditCat">
			  <div class="card" id="addproduct">
			  <div class="card-header" id="headingOne">
				 <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOneEditCat" aria-expanded="true" aria-controls="collapseOne"><center>Edit Career	</center>	</h5> 
			   </div> 

			   <div id="collapseOneEditCat" class="collapse" aria-labelledby="headingOne" data-parent="#collapseOneEditCat">
				<div class="card-body text-light"> 	   
					<form action=""  onSubmit={handleEditCategory}>
					{ 
						categoriesStatus.isLoading ? 
						<div class="spinner-border text-primary" role="status">
						<span class="sr-only">Loading...</span>
						</div>
						:
						<React.Fragment> 
						<div className="form-group"> 
							

							<div className="form-group"> 
								<label htmlfor="category"> Career Name:</label> 
								<select 
								type="text"
								className="form-control"
								id="category"
								name="category"
								onChange={handleChangeSelected}
								>
								<option disabled selected> Select Career </option> 
								{categories.map(category=> {
									return (
									<option value={category._id}>{category.name}</option>
								)
								})
								}

								</select>
							</div>  
								<label htmlfor="name"> New Career Name:</label> 
							<input
								type="text"
								className="form-control"
								id="name"
								name="name"
								value={selectedCategory.name}
								onChange={handleChangeName}

								/>
								</div> 
									
							
								<button className="btn float-right mb-4" id="addproductbtn"> Update Career</button>
						
						</React.Fragment> 
					
							}
					</form> 
				</div> 
				</div> 
			</div> 
		</div>
	 )
}


export default EditCategoryForm; 
