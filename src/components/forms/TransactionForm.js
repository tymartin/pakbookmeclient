import React, {useState, useEffect, Fragment} from 'react'; 
import TransactionHead from './TransactionHead';
 

const TransactionForm = ({transaction, advisers, total, setSelectedDays, selectedDays}) => {

const [transactions, setTransaction] = useState ([])

useEffect(() => {
	fetch('https://bookmaster-server.herokuapp.com/transactions', {
		headers: {
			"Authorization": localStorage.getItem('token')
		}
	})
	.then(data => data.json())
	.then(transactions => setTransaction(transactions))
}, []) 

const[status, setStatus] = useState({
	status : transaction.status
});

const handleChangeSelectStatus = e => {
	setStatus({status : e.target.value})
}

const handleEditStatus = (transactionId) => {
	fetch('https://bookmaster-server.herokuapp.com/transactions/'+transactionId,{
	method : "PUT",
	headers : {
	"Content-Type" : "application/json",
	"Authorization" : localStorage.getItem('token')
},
body : JSON.stringify(status)
	}).then(data => {
	data.json()
	}).then(result => {
	console.log(result)
	})
	}

	
	const[stripes, setStripes] = useState({
		userId : localStorage.getItem('userId'),
		total : total
		})
		
		const handlePay = () => {
		fetch('https://bookmaster-server.herokuapp.com/transactions/stripe',
		{
		method : "POST",
		headers : {
		"Content-Type" : "application/json",
		"Authorization" : localStorage.getItem('token')
		},
		body : JSON.stringify(stripes)
		})
		.then(data => data.json())
		.then(result => {
		window.open(result.receipt_url)
		})
		}
console.log(stripes)
	return (

			<React.Fragment> 
			<div className="container">
				<div class="jumbotron jumbotron-fluid"id="cjumbo">
                <div class="container">
                    <h5 class="display-4 text-center py-8">Transactions</h5>        
                </div>
         	   </div> 
			</div> 
					<div class="accordion col-10 ml-5" id="accordionExample">
					          {
					      	transactions.map( transaction => {
					      		return (
					
								<div class="card">
									<div class="card width-50" id="headingOne">
									<h6 class="mb-0">
										<button class="btn btn-link" 
										type="button" 
										data-toggle="collapse" 
										data-target={"#collapseOne"+transaction._id} 
										aria-expanded="true" 
										aria-controls="collapseOne">

					         	<TransactionHead
					         	transaction={transaction}/> 
					         	
					        		</button>
									</h6>
									</div>

					  <div id={"collapseOne"+transaction._id} class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
					      <div class="card-body width-50"> 
                          <p className="mb-0"> Payment Mode : <strong>{transaction.paymentMode}</strong></p> 
 						<p className="mb-0"> Client Name :<strong> {localStorage.getItem('user')}</strong></p>


						<p className="mb-0"> Status :<strong>{transaction.status}</strong></p>

						{ localStorage.getItem('isLogged') && localStorage.getItem('role') == "admin" ? 
                         <Fragment>
						  	<select onChange={handleChangeSelectStatus} name="status" className="form-control w-25 mt-2 mb-2">
						  		<option value="Canceled">Canceled </option> 
								<option value="On Going">On Going</option> 
						  		<option value="Done">Done</option> 
						  	</select>  
						  	<button onClick={()=>{handleEditStatus(transaction._id)}} className="btn btn-warning mb-2">Update Status</button>
							  </Fragment>
							  :
							  <Fragment>

							  </Fragment>
							  }
 						<table class="table table-striped">

						  <thead>
						    <tr>
						      <th scope="col">Counselor's Name</th>
						      <th scope="col">Subtotal</th>
						      <th scope="col">Appointment Date</th>
							  <th scope="col">Payment Mode</th>
						    </tr>
						  </thead>
						  <tbody>
						  	
								{
									transaction.advisers.map(adviser=> {
										return(
											<tr>
									      		<td>{adviser.name}</td>
									      		<td>{adviser.price}</td>
												<td></td>
												<td>Over the Counter</td>
							   		 		</tr>
							   		 	)
									})
								}
						  		
					
						  </tbody>
						  <tfoot> 
						  	<td className="text-right"> Total: </td> 
						  	<td><strong> {transaction.total}</strong> </td> 
						  	<td></td>
						  </tfoot> 
						</table>  

						{ localStorage.getItem('role') != 'admin' && localStorage.getItem('isLogged') ?
								<Fragment>
								<button className="btn btn-primary w-25 float-right m-2" onClick={handlePay}>
								Pay thru Stripe
								</button>
								</Fragment>
								:
								<Fragment>
								</Fragment>
								}
						

					      </div>
					    </div>
					  </div>
					)})}
					</div>
			</React.Fragment> 
		)
}


export default TransactionForm;
