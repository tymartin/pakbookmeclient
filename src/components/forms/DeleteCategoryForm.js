import React, {useState} from "react"


const DeleteCategoryForm = ({categories}) => {
console.log(categories)
	const [selectedCategory, setSelectedCategory] = useState({

	})
	
		const handleChangeSelected = (e) => {
			let categorySelected = categories.find(category => { 
				if (category._id == e.target.value)
				{
					return category.name
				}
			})
			setSelectedCategory({
				id: e.target.value,
				name: categorySelected.name
			})
		}
	
		const handleDeleteCategory = e => {
			e.preventDefault(); 
	
			fetch("https://bookmaster-server.herokuapp.com/categories/" + selectedCategory.id, {
				method: "DELETE",
				body : JSON.stringify({ name:selectedCategory.name}), 
				headers: {
					"content-Type": "application/json",
					"Authorization" : localStorage.getItem('token')
				}
			})
			.then(data => data.json())
			.then(result => console.log(result))
		}
	
	
	return(

		<div class="accordion pr-3 mt-2" id="accordionDelCat">
		<div class="card" id="addproduct">
		<div class="card-header" id="headingOne">
		   <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOneDelCat" aria-expanded="true" aria-controls="collapseOne"><center>Delete Career	</center>	</h5> 
		 </div> 

		 <div id="collapseOneDelCat" class="collapse" aria-labelledby="headingOne" data-parent="#collapseOneDelCat">
				<div class="card-body text-light"> 	   
					<form action="" onSubmit={handleDeleteCategory} > 
						<div className="form-group"> 
						
							<div className="form-group"> 
								<label htmlfor="category"> Select Career Name:</label> 
									<select 
								type="string"
								className="form-control"
								id="category"
								name="category"
								onChange={handleChangeSelected}
								>
								<option disabled selected> Select Category </option> 
								{categories.map(category=> {
							return (
							<option value={category._id}>{category.name}</option>
								)
								})
								}

								</select>
							</div> 
							
							<button className="btn float-right mb-3" id="addproductbtn"> Delete Career</button> 
							</div> 
					</form> 
				</div> 
			</div> 
		</div>
		</div> 
	 )
}


export default DeleteCategoryForm; 
