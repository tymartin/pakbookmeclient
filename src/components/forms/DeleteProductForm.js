import React, {useState} from "react"


const DeleteProductForm = ({advisers}) => { 
	const [selectedAdviser, setSelectedAdviser] = useState({
		_id: null,
		name: null 
	})

	const handleChange = e => {
		setSelectedAdviser({
			_id: e.target.value 
		})
	}

	const handleDeleteAdviser = e => {
		e.preventDefault();

		let url = 'https://bookmaster-server.herokuapp.com/advisers/' + selectedAdviser._id; 

		fetch(url, {
			method: "DELETE",
			headers: {
				"Authorization": localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(

		<div class="accordion pr-3 mt-2" id="accordionDelProd">
			  <div class="card" id="addproduct">
			  <div class="card-header" id="headingOne">
				 <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOneDelete" aria-expanded="true" aria-controls="collapseOne"><center> Delete Adviser</center>	</h5> 
			   </div> 

			   <div id="collapseOneDelete" class="collapse" aria-labelledby="headingOne" data-parent="#accordionDelProd">
				<div class="card-body text-light"> 	   
					<form action="" onSubmit={handleDeleteAdviser}> 
					
							<div className="form-group"> 
								<form action="" onSubmit={handleDeleteAdviser} > 
								
								<select 
								name="adviserId"
								className="form-control"
								id="adviser"
								name="adviser"
								onChange={handleChange}
								>
								<option selected disabled> Select Adviser </option> 
								{
								advisers.map(adviser=>{
								return (
								
								<option value={adviser._id}> {adviser.name} </option> 
								)
								})}  

								</select>
								</form> 
							</div> 
							
							<button className="btn float-right mb-3" id="addproductbtn"> Delete Adviser</button> 
							
					</form> 
					</div> 
					</div> 
			</div> 
		</div>
	 )
}


export default DeleteProductForm; 
