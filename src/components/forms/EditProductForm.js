import React, {useState} from "react"


const EditProductForm = ({categories, advisers, handleChangeAdvisersStatus, advisersStatus}) => {

	const [selectedAdviser, setSelectedAdviser] = useState({
		name: null, 
		_id: null, 
		price: null, 
		categoryId: null, 
		description: null, 
		schedule: null, 
		categoryName: null 
	
	})
	const handleChangeSelected = (e) => {
			let adviserSelected = advisers.find(adviser => { 
			return (adviser._id == e.target.value) 
			})
			let currCategory = categories.find(category => {
				return category._id === adviserSelected.categoryId
			})
			setSelectedAdviser({
				...adviserSelected,
				image: "https://bookmaster-server.herokuapp.com/" + adviserSelected.image, 
				categoryName: currCategory.name
			})
		}
	
	const handleChangeName = e => {
			setSelectedAdviser ({
				...selectedAdviser,
				[e.target.name] : e.target.value
			})
			console.log(typeof selectedAdviser.image)
		}
	
	const handleChangeFile = e => {
		console.log(e.target.files[0])
		setSelectedAdviser({
			...selectedAdviser,
			image: e.target.files[0]
		})
	
		console.log(selectedAdviser)
	}
	
	const handleEditAdviser = (e) => {
	e.preventDefault();
	console.log(selectedAdviser);
	
	let formData = new FormData();
	
	formData.append('name', selectedAdviser.name);
	formData.append('price', selectedAdviser.price);
	formData.append('categoryId', selectedAdviser.categoryId);
	formData.append('description', selectedAdviser.description);
	formData.append('schedule', selectedAdviser.schedule);
	if(typeof selectedAdviser.image === 'object') {
	formData.append('image', selectedAdviser.image);
	
	}
	console.log(formData.get('image'));
	
	let url = "https://bookmaster-server.herokuapp.com/advisers/"+selectedAdviser._id;
	
	fetch(url, {
	method : "PUT",
	headers : {
	"Authorization" : localStorage.getItem('token')
	},
	body: formData
	})
	.then( data => data.json())
	.then( result => console.log(result))
	}
	

	return(
		
			<div class="accordion pr-3" id="accordionEditProd">
			  <div class="card" id="addproduct">
			  <div class="card-header" id="headingOne">
				 <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><center> Update Adviser</center>	</h5> 
			   </div> 

			   <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionEditProd">
				<div class="card-body text-light"> 	   
				<form action="" onSubmit={handleEditAdviser} > 

					<div className="form-group"> 
						<label htmlfor="advisers"> List of Advisers:</label> 
							<select 
						type="text"
						className="form-control mb-1"
						id="adviser"
						name="adviser"
						onChange={handleChangeSelected}
						>
							<option selected disabled>Select Adviser Here</option>
							{advisers.map(adviser=> {
							return (
							<option value={adviser._id}>{adviser.name}</option>
								)
						})
						}  
						</select>
					</div>  

					
						<label htmlfor="image"> Adviser Image:</label> 
						<hr/>
						<img src={selectedAdviser.image} height="240"/> 
						<hr/>
				
					<div className="form-group"> 
						<label htmlfor="name"> Adviser Name:</label> 
						<input
						type="text"
						className="form-control"
						id="name"
						name="name"
						autoComplete="off"
						value={selectedAdviser.name}
						onChange={handleChangeName}

						/>
					</div> 
					
					<div className="form-group"> 
						<label htmlfor="price"> Consultation Fee:</label> 
						<input
						type="number"
						className="form-control"
						id="price"
						name="price"
						value={selectedAdviser.price}
						onChange={handleChangeName}
					
						/>
					</div>

					<div className="form-group"> 
					<label htmlfor="categoryId"> Specialization:</label> 
					<select
						type="text"
						className="form-control mb-1"
						id="categoryId"
						name="categoryId"
						value={selectedAdviser.categoryId}
						onChange={handleChangeName}
						>
							 {
					categories.map(category=>{
					return (
					category._id === selectedAdviser.categoryId ?
					<option value={category._id}> {category.name} </option> :
					<option value={category._id} selected> {category.name} </option>
					)
					})}  

				
					
					 
						</select> 	
					</div>  

					<div className="form-group"> 
						<label htmlfor="description"> Description:</label> 
						<textarea
						type="string"
						className="form-control"
						id="description"
						name="description"
						value={selectedAdviser.description}
						onChange={handleChangeName}
					
						>
						</textarea>
					</div> 

					<div className="form-group"> 
						<select 
						type="string"
						className="form-control"
						id="schedule"
						name="schedule"
						value={selectedAdviser.schedule}
						onChange={handleChangeName}
						>
						<option value="Mondays-Fridays">Mondays-Fridays</option>
						<option value="Wednesday">Wednesday</option>		
						<option value="Monday-Wednesday">Monday-Wednesday</option>
						<option value="Saturday">Saturday</option>
						</select>
					</div>  
			
					<div className="form-upload"> 
						<input
						type="file"
						name="image"
						id="image"
						className="file"
						onChange={handleChangeFile}

					
						> 
						</input>			
					</div>
					
					<button className="btn mb-3 mt-2 float-right" id="addproductbtn"> Update Details</button>


				</form>
				
				</div> 
				</div> 
		
			  </div> 
		 	</div> 
			
		

		)

}


export default EditProductForm; 
