import React, {useState} from "react"

import './../../style.css'

const LoginForm = ({handleToken, handleUserLogin}) => {

	const [formData, setFormData] = useState({
		email: null,
		password: null 
	})

	const handleChange = e => {
		setFormData({
			...formData, 
			[e.target.name] : e.target.value
		})
	}

	const [result, setResult] = useState({
		message: null,
		successful: null 
	})

	const handleLogin = e => {
		e.preventDefault(); 

		let data = {
			email: formData.email,
			password: formData.password,
		}
		fetch('https://bookmaster-server.herokuapp.com/users/login', {
			method: "POST",
			body: JSON.stringify(data), 
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then( data => data.json())
		.then( user => {
			console.log(user)

			if(user.token) {
			setResult({
				successful: true,
				message: user.message
			})
			localStorage.setItem('role', user.user.role);
			localStorage.setItem('user', user.user.firstname);
			localStorage.setItem('userId', user.user.id);
			localStorage.setItem('token', "Bearer " + user.token);
			localStorage.setItem('isLogged',true);
			window.location.href = "/Catalog" 

			} else {
				setResult({
				 successful: false,
				 message: user.message
				})
			}
		})
	}

	const resultMessage = () => {

		let classlist;
		if(result.successful ===true) {
			classlist = 'alert alert-success'
		} else {
			classlist = 'alert alert-danger'
		}
		return (
			<div className={classlist}> 
			{result.message}
			</div>
			)
	}

	return(


		<React.Fragment> 
            <div className="row"> 
            <div className="col-10 col-md-4 mt-0 mx-auto" > 

			<form action="" onSubmit={handleLogin}> 
				<div className="card mt-5 text-light" id="loginbody">
				{result.successful == null ? "" : resultMessage()}
					<h6 className="card-header"  id="login">Sign In</h6> 
					<div className="card-body px-3" > 
					
						<input
						type="email"
						className="form-control mx-auto mb-3"
						placeholder="Username"
						id="email"
						name="email"
						onChange={handleChange}
						/>

						<input
						type="password"
						className="form-control mx-auto mb-3"
						placeholder="Password"
						id="password"
						name="password"
						onChange={handleChange}
						/>

						<button className="btn mb-1 float-right" id="loginbtn">Login</button> 
					</div> 			
				</div> 
			</form> 
            </div> 
            </div> 
		</React.Fragment> 

		)

}

export default LoginForm; 
