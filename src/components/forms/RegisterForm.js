import React, {useState} from "react"


const RegisterForm = () => {

	const [ formData, setFormData] = useState({
		firstname: "",
		lastname: "",
		email: "",
		password: "",
		confirmPassword: ""
	}); 

	const { firstname, lastname, email, password, confirmPassword} = formData

	const onChangeHandler = e => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
	}

	const handleRegister = e => {
		e.preventDefault();
		if(password !== confirmPassword){
			alert("Passwords do not match")
		} else {
			fetch("https://bookmaster-server.herokuapp.com/users/register", {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				}, 
				body: JSON.stringify(formData)
			})
			.then(data => data.json())
			.then(user => {
				console.log(user.message)
				setFormData({
					firstname: "",
					lastname: "",
					email: "",
					password: "",
					confirmPassword: ""
				})
				if (user.message) {
					let element = document.getElementById("message")
					element.innerHTML = user.message
					element.classList.toggle('d-none')
					setTimeout(function() {
					element.classList.toggle('d-none')
					}, 3000) 
				}
					if (user.successMessage) {
					let element = document.getElementById("smessage")
					element.innerHTML = user.successMessage
					element.classList.toggle('d-none')
					setTimeout(function() {
					element.classList.toggle('d-none')
					}, 9000) 
				}
			})
		}
	}

	

	return(
		<React.Fragment> 

            <div className="row" > 
            <div className="col-10 col-md-4 mt-3 p-0 mx-auto" id="registerbody"> 

			<div className="text-primary"> 
                <div className="alert alert-danger d-none" role="alert" id="message">  </div>
                <div className="alert alert-success d-none" role="alert" id="smessage"> 
					You are Now Registered!
				</div> 
			<h6 className="card-header"  id="registerhead">Sign Up</h6> 
            <div className="card-body"> 
				<form onSubmit={ e=> handleRegister(e)}> 
					<div className="form-group"> 
						<input
						type="text"
						className="form-control"
						id="firstname"
						name="firstname"
                        value={firstname}
                        placeholder="Firstname"
                        autoComplete="off"
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<input
						type="text"
						className="form-control"
						id="lastname"
						name="lastname"
                        value={lastname}
                        placeholder="Lastname"
                        autoComplete="off"
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<input
						type="text"
						className="form-control"
						id="email"
						name="email"
                        value={email} 
                        placeholder="E-mail"
                        autoComplete="off"
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<input
						type="password"
						className="form-control"
						id="password"
						name="password"
                        value={password}
                        placeholder="Password"
                        autoComplete="off"
						onChange={ e => onChangeHandler(e)}
						/>
					</div>

					<div className="form-group"> 
						<input
						type="password"
						className="form-control"
						id="confirmPassword"
						name="confirmPassword"
                        value={confirmPassword}
                        placeholder="Confirm-Password"
                        autoComplete="off"
						onChange={ e => onChangeHandler(e)}
						/>
					</div>

					<button type="submit" className="btn mb-2" id="registerbtn"> Register </button> 
				</form> 
			</div> 
            </div> 
            </div> 
            </div>
		</React.Fragment>

		)

}


export default RegisterForm; 
