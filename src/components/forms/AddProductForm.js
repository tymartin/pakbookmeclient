import React, {useState} from "react"
import DeleteProductForm from './DeleteProductForm';  

const AddProductForm = ({categories}) => {

	const [adviser, setAdviser] = useState({
		name: null,
		description: null,
		price: null,
		categoryId: null,
		schedule: null, 
		image: null
	});
	
	const onChangeText = e => {
		setAdviser({
			...adviser, 
			[e.target.name] : e.target.value
		})
	}
	
	const handleChangeFile = e => {
		console.log(e.target.files[0])
		setAdviser({
			...adviser,
			image: e.target.files[0]
		})
	}
	
	const handleAddAdviser = e => {
		e.preventDefault();
		const formData = new FormData(); 
	
		formData.append('name',adviser.name)
		formData.append('price',adviser.price)
		formData.append('categoryId',adviser.categoryId)
		formData.append('description',adviser.description)
		formData.append('schedule',adviser.schedule)
		formData.append('image',adviser.image)
	
		let url = "https://bookmaster-server.herokuapp.com/advisers/";
			let data = {name: formData}
			let token =localStorage.getItem('token'); 
			fetch(url, {
				method :"POST",
				body: formData, 
				headers: {
					"Authorization" : token
				}
			})
			.then( data=>data.json())
			.then( adviser=>console.log(adviser))
	
	}
	
	return(

		<div class="accordion" id="accordionAddProd">
				<div className="card mr-3"  id="addproduct" > 

				<div class="card-header" id="headingOne">
				 <h5 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOneAddProd" aria-expanded="true" aria-controls="collapseOne"><center>Add New Adviser	</center>	</h5> 
			   </div> 

			   <div id="collapseOneAddProd" class="collapse-show" aria-labelledby="headingOne" data-parent="#collapseOneAddProd">
				<div className="card-body text-light">
				<form action=""  enctype="multipart/form-data" onSubmit={handleAddAdviser} > 
					<div className="form-group"> 
						<input
						type="text"
						className="form-control"
						id="name"
						name="name"
						placeholder="Adviser's Full Name"	
						autoComplete="off"
						onChange={onChangeText}
						/>
					</div> 
					
					<div className="form-group"> 
						<input
						type="number"
						className="form-control"
						id="price"
						name="price"
						placeholder="Consultation Fee"
						onChange={onChangeText}				
						/>
					</div>

					<div className="form-group"> 
						<select 
						type="string"
						className="form-control"
						id="categoryId"
						name="categoryId"
						onChange={onChangeText}
						>
						<option disabled selected> Select Profession </option> 
						{categories.map(category=> {
							return (
							<option value={category._id}>{category.name}</option>
								)
						})
						}
						</select>
					</div>  

					<div className="form-group"> 
						<textarea
						type="string"
						className="form-control"
						id="description"
						name="description"
						placeholder="Counseling Type"
						onChange={onChangeText}
						>
						</textarea>
					</div> 

					<div className="form-group"> 
						<select 
						type="string"
						className="form-control"
						id="schedule"
						name="schedule"
						onChange={onChangeText}
						>
						<option disabled selected> Select Schedule </option> 
						<option value="Mondays-Fridays">Mondays-Fridays</option>
						<option value="Wednesday">Wednesday</option>		
						<option value="Monday-Wednesday">Monday-Wednesday</option>
						<option value="Saturday">Saturday</option>
						</select>
					</div>  
			
					<div className="form-upload"> 
						<input
						type="file"
						name="image"
						id="image"
						className="file"
						onChange={handleChangeFile}
						> 
						</input>			
					</div>	
					
					<button className="btn mb-4 mt-2 float-right" id="addproductbtn"> Add Adviser</button>


				</form>
				</div> 
				</div> 
				</div> 


				</div> 			
		 
		
	

		)

}


export default AddProductForm; 
