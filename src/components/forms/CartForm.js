import React, {useState} from 'react'; 
import counseling from './../../counseling.jpg'; 




const CartForm = ({cart, handleClearCart, handleRemoveItem,payments, total, selectedDays, setSelectedDays}) => {

	const [selectedItem, setSelectedItem] = useState({
		...cart,
		paymentMode : null
	}); 
	console.log(selectedItem)
	const handleCheckout=() => {
		let orders = cart.map(item => {
			return {
				id: item._id,
				qty: selectedDays.length,
				days: selectedDays,
				total: total,
				...selectedItem 
			}
		})
		console.log(orders)
	
		fetch("https://bookmaster-server.herokuapp.com/transactions", {
			method: "POST",
			headers : {
				"Content-Type" : "application/json", 
				"Authorization" : localStorage.getItem('token')
			}, 
			body : JSON.stringify({orders})
		})
		.then(data => data.json())
		.then(result => console.log(result))
		console.log({orders})
	}
	
	

return (
		
		<div className="container">
			<div class="jumbotron jumbotron-fluid"id="cjumbo">
                <div class="container">
                    <h5 class="display-4 text-center py-8">Appointments</h5>        
                </div>
            </div>

			<div className="row">
				
				<div className="col-12">
				{
					cart.length ?
					<React.Fragment>
                
					 	<table class="table  table-responsive col-md-12">
						  <thead class="thead" id="carthead">
						    <tr>
                              <th scope="col"><center>Adviser Name</center></th>
						      <th scope="col"><center>Counseling Type</center></th>
						      <th scope="col"><center>Counseling Fee</center></th>
							  <th scope="col"><center>Days</center></th>
                              <th scope="col"><center>Date</center></th>
                              <th scope="col">Subtotal</th>
							  <th scope="col">Payment Mode</th>
						      <th scope="col"><center>Action</center></th>
						    </tr>
						  </thead>
						  <tbody id="cartbody">
						  {
						  	cart.map( item => {
						  		return (

						  		<tr>
								      <td>{item.name}</td>
								      <td>{item.description}</td>
                                      <td>{item.price}</td>
									  <td> 
									  {selectedDays.length}
									  </td>
                                      <td>
									 
									  </td>
									    
                                      <td>
									  	{item.price * selectedDays.length}
									  </td> 
									  <td>
									  <p>Over the Counter</p>
									  </td>
								      <td><button onClick={()=>{handleRemoveItem(item)}}  className="btn btn-danger">Cancel Appointment</button></td>
						   		 </tr>
									)
								})
							}

						  </tbody>
						  <tfoot id="tablefoot"> 
						  	<td> </td>	
						  	<td className="text-right"> Total </td> 
						  	<td> {total} </td> 
						  	<td></td>
							<td>
							
							</td>
							<td></td>
							<td></td>
							<td><center><button onClick={handleCheckout} className="btn btn-success">Checkout</button> </center> </td>	
						  </tfoot> 
						</table>

						<button onClick={handleClearCart} className="btn" id="clearcart">Cancel All Appointments</button>
					</React.Fragment> : <p id="noOrders"> <center> 'No Scheduled Appointments To Show'</center></p>
					}

				</div> 
			</div> 
		</div> 


)
}

export default CartForm;
