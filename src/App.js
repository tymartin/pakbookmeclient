import React, { useEffect, useState, Fragment }  from 'react'; 
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import Navbar from './components/layouts/Navbar'; 
import Home from './components/layouts/Home'; 
import Catalog from './components/Catalog'; 
import StartDayPicker from './components/layouts/StartDayPicker'; 

//CSS
import './style.css'

//Forms 
import LoginForm from './components/forms/LoginForm';
import RegisterForm from './components/forms/RegisterForm';
import AdimnPanel from './components/forms/AdminPanel'; 
import CartForm from './components/forms/CartForm'; 
import TransactionForm from './components/forms/TransactionForm'; 

function App() { 

//states
const [advisers, setAdvisers] = useState([])
const [cart, setCart] = useState([])
const [selectedDays, setSelectedDays] = useState([])
const [transaction, setTransaction] = useState([])
const [total, setTotal] = useState(0)
const handleAddToCart = (adviser) => {
  let matched = cart.find(item => {
    return item._id === adviser._id; 
  })
  console.log(matched)
 if(!matched){
  setCart([
    ...cart,
    adviser
    ])
}
}

const handleClearCart = () => {
  setCart([])
}

const handleRemoveItem = (itemTobeRemove)=> {
  let updatedCart = cart.filter( item=>item !== itemTobeRemove)
  setCart(updatedCart)
}
const [advisersStatus, setAdvisersStatus] = useState({
  lastUpdated: null, 
  status: null
})
const [categories, setCategories] = useState([])
const [categoriesStatus, setCategoriesStatus] = useState({
  lastUpdated: null, 
  status: null, 
  isLoading: false
})
const [user, setUser] = useState ({
  firstname: null, lastname: null, role: null, id: null 
}
) 
const [token, setToken] = useState(""); 

useEffect(()=> {
let total = 0;
cart.forEach(item=>{
  total += item.price * selectedDays.length
})
setTotal(total)
},[cart])

useEffect(()=>{
  fetch("https://bookmaster-server.herokuapp.com/categories", {
    method: "GET"
  })
   .then(res=>res.json())
    .then(data => {
      setCategories([...data])
      setCategoriesStatus({isLoading : false})
      })
},[categoriesStatus.isLoading])

useEffect(() => {
   fetch("https://bookmaster-server.herokuapp.com/advisers")
   .then(res=>res.json())
   .then(data => {
    setAdvisers([...data])
   })

   fetch("https://bookmaster-server.herokuapp.com/categories")
   .then(res=>res.json())
    .then(data => {
      setCategories([...data])
      })
    },[]); 

//change state methods 
const handleChangeCategoriesStatus = (status) => {
setCategoriesStatus(status)
}

const handleChangeAdvisersStatus = (status) => {
setAdvisersStatus(status)
}

  return (
    <Router> 
     <Navbar/>
        <Switch> 
        <Route exact path ="/">
                <Home
                advisers={advisers}
                /> 
        </Route>
        { localStorage.getItem('isLogged') && localStorage.getItem('role') == "admin" ?
         <Fragment> 
              <Route exact path ="/Catalog">
                <Catalog 
                advisers={advisers}
                handleAddToCart={handleAddToCart}
                setSelectedDays={setSelectedDays}
                selectedDays={selectedDays}
                /> 
            </Route>
            <Route exact path ="/admin-panel">
                <AdimnPanel
                 categories={categories}
                 handleChangeCategoriesStatus={handleChangeCategoriesStatus}
                 categoriesStatus={categoriesStatus} 
                 advisers={advisers}
                 handleChangeAdvisersStatus={handleChangeAdvisersStatus}
                 advisersStatus={advisersStatus}
   
                /> 
            </Route>

            <Route exact path ="/appointment">
                <CartForm
                cart={cart}
                handleClearCart={handleClearCart}
                handleRemoveItem={handleRemoveItem}
                total={total} 
                 selectedDays ={selectedDays}
                 setSelectedDays={setSelectedDays}
                /> 
            </Route>

            <Route exact path ="/transaction">
                <TransactionForm 
                cart={cart}
                total={total}
                setSelectedDays={setSelectedDays}
                transaction={transaction}/>     
                /> 
            </Route>
         </Fragment>
         :
         <Fragment> 
           { localStorage.getItem('isLogged') && localStorage.getItem('role') == "user" ? 
            <Fragment> 
                   <Route exact path ="/Catalog">
                <Catalog 
                advisers={advisers}
                handleAddToCart={handleAddToCart}
                setSelectedDays={setSelectedDays}
                selectedDays={selectedDays}
                /> 
            </Route> 

            <Route exact path ="/appointment">
                <CartForm
                cart={cart}
                handleClearCart={handleClearCart}
                handleRemoveItem={handleRemoveItem}
                total={total} 
                 selectedDays ={selectedDays}
                 setSelectedDays={setSelectedDays}
                /> 
            </Route>

            <Route exact path ="/transaction">
                <TransactionForm 
                cart={cart}
                total={total}
                setSelectedDays={setSelectedDays}
                transaction={transaction}/>     
                /> 
            </Route>
            </Fragment> 
            :
            <Fragment> 
                   
            <Route exact path ="/login">
                <LoginForm/> 
            </Route>

            <Route exact path ="/register">
                <RegisterForm/> 
            </Route>

         
            </Fragment>
          }

         </Fragment>
        }
        </Switch>
    </Router> 
    
  )
}

export default App;
